<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Pensiune</title>

<jsp:include page="/jsp/common/head_imports.jsp"/>

</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

<jsp:include page="/jsp/common/sidebar.jsp"/>

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">           
            <div class="card card-primary card-outline">
              <div class="card-header">
                <h5 class="m-0">Adaugare client</h5>
              </div>
              <div class="card-body">
              
              

<form:form action="${pageContext.request.contextPath}/adaugare-client-save.htm" method="post" modelAttribute="client">
<div class="form-group">
	<label for="nume">Nume </label><br/>
		<form:input path = "nume" class="form-control"/>
</div>
<div class="form-group">
	<label for="prenume">Prenume </label><br/>
		<form:input path = "prenume" class="form-control"/>
</div>
<div class="form-group">
	<label for="telefon">Telefon </label><br/>
		<form:input path = "telefon" class="form-control"/>
</div>
<div class="form-group">
	<label for="email">Email </label><br/>
		<form:input path = "email" class="form-control"/>
</div>
<div class="form-group">
	<label for="dataRegistrari">Data registrari </label><br/>
		<form:input path = "dataRegistrari" class="form-control"/>
</div>
<div class="form-group">
	<label for="dataIesiri">Data iesiri </label><br/>
		<form:input path = "dataIesiri" class="form-control"/>
</div>
<div class="form-group">
    <label for="idCamera">Camera </label> <br/>
         <form:select path = "idCamera" class="form-control select2" name="camerele">
   		 <c:forEach items="${camerele}" var="camerele">
        <form:option value="${camerele.id}">${camerele.nr}</form:option>
    </c:forEach>
                  </form:select>
                </div>
<input type="submit" value="Salveaza" class="btn btn-primary" /><br/>
</form:form>
<p/> <br/>
<a href="<c:url value="/listclient.htm"/>" class="btn btn-danger">Inapoi la lista</a>





</div>
            </div>
          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<jsp:include page="/jsp/common/footer.jsp"/>

</div>
<!-- ./wrapper -->

<jsp:include page="/jsp/common/bottom_imports.jsp"/>

</body>
</html>