<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 

<!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a> 
      </li>
    </ul>
     <ul class="navbar-nav">
      <li class="nav-item">
   		 <a class="nav-link" href="<c:url value="/index.htm"/>" role="button"><i class="fas fa-home"></i>Home</a> 
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">  
      <li class="nav-item">
        <a class="nav-link" data-widget="fullscreen" href="#" role="button">
          <i class="fas fa-expand-arrows-alt"></i>
        </a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <a href="<c:url value="/index.htm"/>" class="brand-link">
      <span class="brand-text font-weight-light"><strong>Pensiune</strong></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">

      <!-- Sidebar Menu -->
      <nav class="mt-2">
            
            <a href="<c:url value="/listangajat.htm"/>" class="nav-link">
              <i class="nav-icon fas fa-table"></i>
                Angajat List
            </a>
      </nav>
      <nav class="mt-2">
            
            <a href="<c:url value="/listcamera.htm"/>" class="nav-link">
              <i class="nav-icon fas fa-table"></i>
                Camera List
            </a>
      </nav>
      <nav class="mt-2">
            
            <a href="<c:url value="/listclient.htm"/>" class="nav-link">
              <i class="nav-icon fas fa-table"></i>
                Client List
            </a>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
