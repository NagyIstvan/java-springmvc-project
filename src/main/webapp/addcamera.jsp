<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Pensiune</title>

<jsp:include page="/jsp/common/head_imports.jsp"/>

</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

<jsp:include page="/jsp/common/sidebar.jsp"/>

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">           
            <div class="card card-primary card-outline">
              <div class="card-header">
                <h5 class="m-0">Adaugare camera</h5>
              </div>
              <div class="card-body">
              
              
              

<form:form action="${pageContext.request.contextPath}/adaugare-camera-save.htm" method="post" modelAttribute="camera">

<div class="form-group">
	<label for="nr">Nr </label><br/>
		<form:input path = "nr" class="form-control"/>
</div>
<div class="form-group">
	<label for="pat">Pat </label><br/>
		<form:input path = "pat" class="form-control"/>
</div>
<div class="form-group">
	<label for="baie">Baie </label><br/>
		<form:input path = "baie" class="form-control"/>
</div>
<div class="form-group">
	<label for="includeMasa">Masa/Mese </label><br/>
		<form:input path = "includeMasa" class="form-control"/>
</div>
<div class="form-group">
	<label for="cost">Cost </label><br/>
		<form:input path = "cost" class="form-control"/>
</div>
<input type="submit" value="Salveaza" class="btn btn-primary" /><br/>
</form:form>
<p/> <br/>
<a href="<c:url value="/listcamera.htm"/>" class="btn btn-danger">Inapoi la lista</a>





</div>
            </div>
          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<jsp:include page="/jsp/common/footer.jsp"/>

</div>
<!-- ./wrapper -->

<jsp:include page="/jsp/common/bottom_imports.jsp"/>

</body>
</html>