<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 

<!DOCTYPE html>
<html>
<head>

<meta charset="ISO-8859-1">
<title>Pensiune</title>
<jsp:include page="/jsp/common/head_imports.jsp"/>

 
</head>

<body class="hold-transition sidebar-mini">

<div class="wrapper">
<jsp:include page="/jsp/common/sidebar.jsp"/>
 
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">           
            <div class="card card-primary card-outline">
              <div class="card-header"> 
                <h5 class="m-0">Home</h5>
              </div>
              <div class="card-body">
              
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3><c:out value="${clientActual}"/></h3>

                <p>Client actual</p>
              </div>
              <a href="<c:url value="/listclient.htm"/>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3><c:out value="${clientTotal}"/><sup style="font-size: 20px"></sup></h3>

                <p>Client total</p>
              </div>
              <a href="<c:url value="/listclient.htm"/>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3><c:out value="${cameraGoala}"/></h3>

                <p>Camera goala</p>
              </div>
              <a href="<c:url value="/listcamera.htm"/>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3><c:out value="${cameraTotal}"/></h3>

                <p>Camera total</p>
              </div>
              <a href="<c:url value="/listcamera.htm"/>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
        </div>
					
					
					                
              </div>
            </div>
          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<jsp:include page="/jsp/common/footer.jsp"/>

</div>
<!-- ./wrapper -->

<jsp:include page="/jsp/common/bottom_imports.jsp"/>

</body>
</html>