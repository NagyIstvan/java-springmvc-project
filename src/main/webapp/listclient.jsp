<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Pensiune</title>

<jsp:include page="/jsp/common/head_imports.jsp"/>

</head>

<body class="hold-transition sidebar-mini">
<div class="wrapper">

<jsp:include page="/jsp/common/sidebar.jsp"/>
 
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">           
            <div class="card card-primary card-outline">
              <div class="card-header">
                <h5 class="m-0">Lista clienti</h5>
              </div>
              <div class="card-body">
                
					<table class="table table-striped">
					<tr>
						<td><strong>Nume</strong></td>
						<td><strong>Prenume</strong></td>
						<td><strong>Telefon</strong></td>
						<td><strong>Email</strong></td>
						<td><strong>Data registrari</strong></td>
						<td><strong>Data iesiri</strong></td>
						<td><strong>Camera</strong></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<c:forEach var="st" items="${clienti}">
					<tr>
					<td><c:out value="${st.nume}"></c:out></td> 
					<td><c:out value="${st.prenume}"></c:out></td> 
					<td><c:out value="${st.telefon}"></c:out></td> 
					<td><c:out value="${st.email}"></c:out></td> 
					<td><c:out value="${st.dataRegistrari}"></c:out></td> 
					<td><c:out value="${st.dataIesiri}"></c:out></td> 
					<td><c:out value="${camerele[st.idCamera]}"></c:out></td>
					<td><a href="<c:url value="/detaliiclient.htm?id=${st.id}"/>" class="btn btn-info">Detalii</a></td>
					<td><a href="<c:url value="/editare-client.htm?id=${st.id}"/>" class="btn btn-info">Editare</a></td>
					<td><a href="<c:url value="/deleteclient.htm?id=${st.id}"/>" class="btn btn-danger">Delete</a></td>
					
					</tr>
					</c:forEach>
					</table>
					<p/> <br/>
					<a href="<c:url value="/adaugare-client.htm"/>" class="btn btn-success">Adaugare Client</a>
					                
              </div>
            </div>
          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<jsp:include page="/jsp/common/footer.jsp"/>

</div>
<!-- ./wrapper -->

<jsp:include page="/jsp/common/bottom_imports.jsp"/>

</body>
</html>