<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Pensiune</title>

<jsp:include page="/jsp/common/head_imports.jsp"/>

</head>

<body class="hold-transition sidebar-mini">
<div class="wrapper">

<jsp:include page="/jsp/common/sidebar.jsp"/>
 
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">           
            <div class="card card-primary card-outline">
              <div class="card-header">
                <h5 class="m-0">Lista angajati</h5>
              </div>
              <div class="card-body">
                
					<table class="table table-striped">
					<tr>

						<td><strong>Nume</strong></td>
						<td><strong>Prenume</strong></td>
						<td><strong>Telefon</strong></td>
						<td><strong>Email</strong></td>
						<td><strong>Adresa</strong></td>
						<td><strong>Salariu</strong></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<c:forEach var="st" items="${angajati}">
					<tr>
					<td><c:out value="${st.nume}"></c:out></td> 
					<td><c:out value="${st.prenume}"></c:out></td> 
					<td><c:out value="${st.telefon}"></c:out></td> 
					<td><c:out value="${st.email}"></c:out></td> 
					<td><c:out value="${st.adresa}"></c:out></td> 
					<td><c:out value="${st.salariu}"></c:out></td> 
					<td><a href="<c:url value="/detaliiangajat.htm?id=${st.id}"/>" class="btn btn-info">Detalii</a></td>
					<td><a href="<c:url value="/editare-angajat.htm?id=${st.id}"/>" class="btn btn-info">Editare</a></td>
					<td><a href="<c:url value="/deleteangajat.htm?id=${st.id}"/>" class="btn btn-danger">Delete</a></td>
					
					</tr>
					</c:forEach>
					</table>
					<p/> <br/>
					<a href="<c:url value="/adaugare-angajat.htm"/>" class="btn btn-success">Adaugare Angajat</a>
					                
              </div>
            </div>
          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<jsp:include page="/jsp/common/footer.jsp"/>

</div>
<!-- ./wrapper -->

<jsp:include page="/jsp/common/bottom_imports.jsp"/>

</body>
</html>