<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Pensiune</title>
<jsp:include page="/jsp/common/head_imports.jsp"/>

</head>

<body class="hold-transition sidebar-mini">
<div class="wrapper">

<jsp:include page="/jsp/common/sidebar.jsp"/>

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">           
            <div class="card card-primary card-outline">
              <div class="card-header">
                <h5 class="m-0">Detalii camera</h5>
              </div>
              <div class="card-body">
              
              
              

Id: <c:out value="${camera.id}"/> <br/>
Nr: <c:out value="${camera.nr}"/> <br/>
Pat: <c:out value="${camera.pat}"/> <br/>
Baie: <c:out value="${camera.baie}"/> <br/>
Masa/Mese: <c:out value="${camera.includeMasa}"/> <br/>
Cost: <c:out value="${camera.cost} RON/ZI"/> <br/>
<c:if test = "${idClient>0}">
Ocupat: <a href="<c:url value="/detaliiclient.htm?id=${idClient}"/>"> <c:out value="${numeClient}"/></a>
</c:if>
<br/><br/>

<a href="<c:url value="/listcamera.htm"/>" class="btn btn-danger">Inapoi la lista</a>






</div>
            </div>
          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<jsp:include page="/jsp/common/footer.jsp"/>

</div>
<!-- ./wrapper -->

<jsp:include page="/jsp/common/bottom_imports.jsp"/>

</body>
</html>