<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Pensiune</title>

<jsp:include page="/jsp/common/head_imports.jsp"/>

</head>

<body class="hold-transition sidebar-mini">
<div class="wrapper">

<jsp:include page="/jsp/common/sidebar.jsp"/>
 
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">           
            <div class="card card-primary card-outline">
              <div class="card-header">
                <h5 class="m-0">Lista camerei</h5>
              </div>
              <div class="card-body">
                
					<table class="table table-striped">
					<tr>
						<td><strong>Nr</strong></td>
						<td><strong>Pat</strong></td>
						<td><strong>Baie</strong></td>
						<td><strong>Masa/Mese</strong></td>
						<td><strong>Cost</strong></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<c:forEach var="st" items="${camerele}">
					<tr>
					<td><c:out value="${st.nr}"></c:out></td> 
					<td><c:out value="${st.pat}"></c:out></td> 
					<td><c:out value="${st.baie}"></c:out></td> 
					<td><c:out value="${st.includeMasa}"></c:out></td> 
					<td><c:out value="${st.cost}"></c:out></td> 
					<td><a href="<c:url value="/detaliicamera.htm?id=${st.id}"/>" class="btn btn-info">Detalii</a></td>
					<td><a href="<c:url value="/editare-camera.htm?id=${st.id}"/>" class="btn btn-info">Editare</a></td>
					<td><a href="<c:url value="/deletecamera.htm?id=${st.id}"/>" class="btn btn-danger">Delete</a></td>
					
					</tr>
					</c:forEach>
					</table>
					<p/> <br/>
					<a href="<c:url value="/adaugare-camera.htm"/>" class="btn btn-success">Adaugare Camera</a>
					                
              </div>
            </div>
          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<jsp:include page="/jsp/common/footer.jsp"/>

</div>
<!-- ./wrapper -->

<jsp:include page="/jsp/common/bottom_imports.jsp"/>

</body>
</html>