<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Pensiune</title>
<jsp:include page="/jsp/common/head_imports.jsp"/>

</head>

<body class="hold-transition sidebar-mini">
<div class="wrapper">

<jsp:include page="/jsp/common/sidebar.jsp"/>

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">           
            <div class="card card-primary card-outline">
              <div class="card-header">
                <h5 class="m-0">Detalii client</h5>
              </div>
              <div class="card-body">
              
              
              

Id: <c:out value="${client.id}"/> <br/>
Nume: <c:out value="${client.nume}"/> <br/>
Prenume: <c:out value="${client.prenume}"/> <br/>
Telefon: <c:out value="${client.telefon}"/> <br/>
Email: <c:out value="${client.email}"/> <br/>
Data registrari: <c:out value="${client.dataRegistrari}"/> <br/>
Data iesiri: <c:out value="${client.dataIesiri}"/> <br/>
Camera: <a href="<c:url value="/detaliicamera.htm?id=${client.idCamera}"/>"> <c:out value="${nrCamera}"/></a> <br/>
Suma de platit (RON*ZI): <c:out value="${cost}"/> * <c:out value="${nrDays}"/> = <c:out value="${cost*nrDays} RON"/> <br/> <p/>

<br/>

<a href="<c:url value="/listclient.htm"/>" class="btn btn-danger">Inapoi la lista</a>






</div>
            </div>
          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<jsp:include page="/jsp/common/footer.jsp"/>

</div>
<!-- ./wrapper -->

<jsp:include page="/jsp/common/bottom_imports.jsp"/>

</body>
</html>