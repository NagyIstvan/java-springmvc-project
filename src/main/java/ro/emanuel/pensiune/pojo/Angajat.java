package ro.emanuel.pensiune.pojo;

public class Angajat {

	private int id;
	private String nume;
	private String prenume;
	private String telefon;
	private String email;
	private String adresa;
	private int salariu;
	
	public Angajat(int id, String nume, String prenume, String telefon, String email, String adresa, int salariu) {
		super();
		this.id = id;
		this.nume = nume;
		this.prenume = prenume;
		this.telefon = telefon;
		this.email = email;
		this.adresa = adresa;
		this.salariu = salariu;
	}

	public Angajat() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNume() {
		return nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	public String getPrenume() {
		return prenume;
	}

	public void setPrenume(String prenume) {
		this.prenume = prenume;
	}

	public String getTelefon() {
		return telefon;
	}

	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAdresa() {
		return adresa;
	}

	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}

	public int getSalariu() {
		return salariu;
	}

	public void setSalariu(int salariu) {
		this.salariu = salariu;
	}
	
	
}