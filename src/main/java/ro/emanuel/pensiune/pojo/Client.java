package ro.emanuel.pensiune.pojo;

public class Client {
	// POJO = Plain Old Java Object

	private int id;
	private String nume;
	private String prenume;
	private String telefon;
	private String email;
	private String dataRegistrari;
	private String dataIesiri;
	private int idCamera;
	
	public Client(int id, String nume, String prenume, String telefon, String email, String dataRegistrari,
			String dataIesiri, int idCamera) {
		super();
		this.id = id;
		this.nume = nume;
		this.prenume = prenume;
		this.telefon = telefon;
		this.email = email;
		this.dataRegistrari = dataRegistrari;
		this.dataIesiri = dataIesiri;
		this.idCamera = idCamera;
	}

	public Client() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNume() {
		return nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	public String getPrenume() {
		return prenume;
	}

	public void setPrenume(String prenume) {
		this.prenume = prenume;
	}

	public String getTelefon() {
		return telefon;
	}

	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDataRegistrari() {
		return dataRegistrari;
	}

	public void setDataRegistrari(String dataRegistrari) {
		this.dataRegistrari = dataRegistrari;
	}

	public String getDataIesiri() {
		return dataIesiri;
	}

	public void setDataIesiri(String dataIesiri) {
		this.dataIesiri = dataIesiri;
	}

	public int getIdCamera() {
		return idCamera;
	}

	public void setIdCamera(int idCamera) {
		this.idCamera = idCamera;
	}

	

}
