package ro.emanuel.pensiune.pojo;

public class Camera {
	
	private int id;
	private String nr;
	private int pat;
	private int baie;
	private int includeMasa;
	private int cost;
	
	public Camera(int id, String nr, int pat, int baie, int includeMasa, int cost) {
		super();
		this.id = id;
		this.nr = nr;
		this.pat = pat;
		this.baie = baie;
		this.includeMasa = includeMasa;
		this.cost = cost;
	}

	public Camera() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNr() {
		return nr;
	}

	public void setNr(String nr) {
		this.nr = nr;
	}

	public int getPat() {
		return pat;
	}

	public void setPat(int pat) {
		this.pat = pat;
	}

	public int getBaie() {
		return baie;
	}

	public void setBaie(int baie) {
		this.baie = baie;
	}

	public int getIncludeMasa() {
		return includeMasa;
	}

	public void setIncludeMasa(int includeMasa) {
		this.includeMasa = includeMasa;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}


}
