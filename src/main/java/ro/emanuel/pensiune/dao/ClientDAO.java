package ro.emanuel.pensiune.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

import ro.emanuel.pensiune.helpers.DBHelper;
import ro.emanuel.pensiune.pojo.Client;

public class ClientDAO {
	
	public static long getDays(int id) throws SQLException {

		String select = "SELECT "
				+ " Year(data_iesiri) as year, Month(data_iesiri) as month, Day(data_iesiri) as day"
				+", Year(data_registrari) as year2, Month(data_registrari) as month2, Day(data_registrari) as day2"
				+ " FROM clienti where id = ?";

		Connection con = DBHelper.getConnection();

		PreparedStatement stmt = con.prepareStatement(select);
		long result = 0;
		stmt.setInt(1, id);
		ResultSet rs = stmt.executeQuery();
		if (rs.next()) {

			int year = rs.getInt("year");
			int month = rs.getInt("month");
			int day = rs.getInt("day");
			int year2 = rs.getInt("year2");
			int month2 = rs.getInt("month2");
			int day2 = rs.getInt("day2");
			
			LocalDate date = LocalDate.of(year, month, day);
			LocalDate date2 = LocalDate.of(year2, month2, day2);
			
			result = ChronoUnit.DAYS.between(date2, date);
	}
		DBHelper.closeConnection();
		return result;
	}
	
	public static ArrayList<Client> getActualClienti() throws SQLException {

		String select = "SELECT *,"
				+ " Year(data_iesiri) as year, Month(data_iesiri) as month, Day(data_iesiri) as day"
				+ " FROM clienti";

		Connection con = DBHelper.getConnection();

		PreparedStatement stmt = con.prepareStatement(select);

		ArrayList<Client> result = new ArrayList<Client>();
		ResultSet rs = stmt.executeQuery();
		while (rs.next()) {

			int id = rs.getInt("id");
			String nume = rs.getString("nume");
			String prenume = rs.getString("prenume");
			String telefon = rs.getString("telefon");
			String email = rs.getString("email");
			String dataRegistrari = rs.getString("data_registrari");
			String dataIesiri = rs.getString("data_iesiri");
			int idCamera = rs.getInt("id_camera");
			int year = rs.getInt("year");
			int month = rs.getInt("month");
			int day = rs.getInt("day");
			
			LocalDate date = LocalDate.of(year, month, day);
			LocalDate today = LocalDate.now();
				

			if (date.isAfter(today)) {
				{Client c = new Client(id,nume, prenume, telefon, email, dataRegistrari,dataIesiri,idCamera);
				result.add(c);
				}
			}
	}
		DBHelper.closeConnection();
		return result;
	}

	public static Client getClientById(int id) throws SQLException {
		
		String select = "select * from clienti where id=?";
		Connection con=DBHelper.getConnection();
		PreparedStatement stmt = con.prepareStatement(select);
		stmt.setInt(1, id);
		ResultSet rs =stmt.executeQuery();
		Client result = null;
		if (rs.next()) {
			String nume = rs.getString("nume");
			String prenume = rs.getString("prenume");
			String telefon = rs.getString("telefon");
			String email = rs.getString("email");
			String dataRegistrari = rs.getString("data_registrari");
			String dataIesiri = rs.getString("data_iesiri");
			int idCamera = rs.getInt("id_camera");
			result = new Client(id, nume, prenume, telefon, email, dataRegistrari, dataIesiri, idCamera);
		}
		DBHelper.closeConnection();
		return result;
	}
	
	public static ArrayList<Client> getClientByName(String nameQuery) throws SQLException{
		String select = "select * from clienti where nume LIKE ?";
		Connection con = DBHelper.getConnection();
		PreparedStatement stmt = con.prepareStatement(select);
		stmt.setString(1, "%"+ nameQuery+"%");
		ArrayList<Client> result = new ArrayList<Client>();
		ResultSet rs = stmt.executeQuery();
		while (rs.next()) {

			int id = rs.getInt("id");
			String nume = rs.getString("nume");
			String prenume = rs.getString("prenume");
			String telefon = rs.getString("telefon");
			String email = rs.getString("email");
			String dataRegistrari = rs.getString("data_registrari");
			String dataIesiri = rs.getString("data_iesiri");
			int idCamera = rs.getInt("id_camera");

			Client s = new Client(id, nume, prenume, telefon, email, dataRegistrari, dataIesiri, idCamera);

			result.add(s);
		}
		DBHelper.closeConnection();
		return result;
	}
	
	public static ArrayList<Client> getClienti() throws SQLException {

		String select = "SELECT * FROM `clienti` ORDER BY `clienti`.`data_iesiri`  DESC";

		Connection con = DBHelper.getConnection();

		PreparedStatement stmt = con.prepareStatement(select);

		ArrayList<Client> result = new ArrayList<Client>();
		ResultSet rs = stmt.executeQuery();
		while (rs.next()) {

			int id = rs.getInt("id");
			String nume = rs.getString("nume");
			String prenume = rs.getString("prenume");
			String telefon = rs.getString("telefon");
			String email = rs.getString("email");
			String dataRegistrari = rs.getString("data_registrari");
			String dataIesiri = rs.getString("data_iesiri");
			int idCamera = rs.getInt("id_camera");

			Client s = new Client(id, nume, prenume, telefon, email, dataRegistrari, dataIesiri, idCamera);

			result.add(s);
		}
		DBHelper.closeConnection();
		return result;
	}
	
	public static void createClient(String nume,String prenume,String telefon, String email, String dataRegistrari, String dataIesiri, int idCamera) throws SQLException{
		String insert="insert into clienti (nume,prenume,telefon,email,data_registrari, data_iesiri, id_camera)"
				+ "value(?,?,?,?,?,?,?);";
		
		Connection con = DBHelper.getConnection();
		
		PreparedStatement stmt = con.prepareStatement(insert);
		
		stmt.setString(1,nume);
		stmt.setString(2, prenume);
		stmt.setString(3,telefon);
		stmt.setString(4, email);
		stmt.setString(5, dataRegistrari);
		stmt.setString(6, dataIesiri);
		stmt.setInt(7, idCamera);
		
		
		stmt.executeUpdate();
		
		DBHelper.closeConnection();
	}
	
	public static void createClient(Client s) throws SQLException {
		createClient(s.getNume(),s.getPrenume(),s.getTelefon(),s.getEmail(),s.getDataRegistrari(),s.getDataIesiri(),s.getIdCamera());
	}
	
	public static void updateClient(Client s) throws SQLException {
		String update = "update clienti set nume=?, prenume=?,telefon=?, email=?,data_registrari=?,data_iesiri=?,id_camera=? where id=?";
	
		Connection con = DBHelper.getConnection();

		PreparedStatement stmt = con.prepareStatement(update);
		
		stmt.setString(1, s.getNume());
		stmt.setString(2, s.getPrenume());
		stmt.setString(3, s.getTelefon());
		stmt.setString(4, s.getEmail());
		stmt.setString(5, s.getDataRegistrari());
		stmt.setString(6, s.getDataIesiri());
		stmt.setInt(7,s.getIdCamera());
		stmt.setInt(8, s.getId());
		
		stmt.executeUpdate();
		
		DBHelper.closeConnection();
				
	}
	public static void deleteClient(int id) throws SQLException{
		String delete = "delete from clienti where id=?";
		
		Connection con = DBHelper.getConnection();

		PreparedStatement stmt = con.prepareStatement(delete);
		
		stmt.setInt(1, id);
		
		stmt.executeUpdate();
		
		DBHelper.closeConnection();
	}
	
	public static void deleteClient(Client s) throws SQLException {
		
		deleteClient(s.getId());
	}
}
