package ro.emanuel.pensiune.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

import ro.emanuel.pensiune.helpers.DBHelper;
import ro.emanuel.pensiune.pojo.Camera;

public class CameraDAO {
	
	
public static ArrayList<Camera> getFreeCamerele() throws SQLException {	
	
	String select = "SELECT camera.*, clienti.nume,"
			+ " Year(clienti.data_iesiri) as year, Month(clienti.data_iesiri) as month, Day(clienti.data_iesiri) as day"
			+ " FROM camera LEFT JOIN clienti on camera.id = clienti.id_camera";
	Connection con=DBHelper.getConnection();
	PreparedStatement stmt = con.prepareStatement(select);

	ArrayList<Camera> result = new ArrayList<Camera>();
	ResultSet rs = stmt.executeQuery();
	while (rs.next()) {

		int id = rs.getInt("id");
		String nr = rs.getString("nr");
		int pat = rs.getInt("pat");
		int baie = rs.getInt("baie");
		int includeMasa = rs.getInt("include_masa");
		int cost = rs.getInt("cost");
		String nume = rs.getString("nume");
		int year = rs.getInt("year");
		int month = rs.getInt("month");
		int day = rs.getInt("day");
		
		if(nume != null) {
		LocalDate date = LocalDate.of(year, month, day);
		LocalDate today = LocalDate.now();
			

		if (date.isBefore(today)) {
			{Camera s = new Camera(id,nr, pat, baie, includeMasa, cost);
			result.add(s);
		}	
	}
}	
		
		if(nume == null)
		{Camera s = new Camera(id,nr, pat, baie, includeMasa, cost);
		result.add(s);
	}	
}
	DBHelper.closeConnection();
	return result;
}
public static Camera getCameraById(int id) throws SQLException {
		
		String select = "select * from camera where id=?";
		Connection con=DBHelper.getConnection();
		PreparedStatement stmt = con.prepareStatement(select);
		stmt.setInt(1, id);
		ResultSet rs =stmt.executeQuery();
		Camera result = null;
		if (rs.next()) {
			String nr = rs.getString("nr");
			int pat = rs.getInt("pat");
			int baie = rs.getInt("baie");
			int includeMasa = rs.getInt("include_masa");
			int cost = rs.getInt("cost");
			result = new Camera(id, nr, pat, baie, includeMasa, cost);
		}
		DBHelper.closeConnection();
		return result;
	}
	
	public static ArrayList<Camera> getCameraByName(String nameQuery) throws SQLException{
		String select = "select * from camera where nume LIKE ?";
		Connection con = DBHelper.getConnection();
		PreparedStatement stmt = con.prepareStatement(select);
		stmt.setString(1, "%"+ nameQuery+"%");
		ArrayList<Camera> result = new ArrayList<Camera>();
		ResultSet rs = stmt.executeQuery();
		while (rs.next()) {

			int id = rs.getInt("id");
			String nr= rs.getString("nr");
			int pat = rs.getInt("pat");
			int baie = rs.getInt("baie");
			int includeMasa = rs.getInt("include_masa");
			int cost = rs.getInt("cost");

			Camera s = new Camera(id, nr, pat, baie, includeMasa, cost);

			result.add(s);
		}
		DBHelper.closeConnection();
		return result;
	}
	
	public static ArrayList<Camera> getCamerele() throws SQLException {

		String select = "SELECT * FROM `camera` ORDER BY `camera`.`nr` ASC";

		Connection con = DBHelper.getConnection();

		PreparedStatement stmt = con.prepareStatement(select);

		ArrayList<Camera> result = new ArrayList<Camera>();
		ResultSet rs = stmt.executeQuery();
		while (rs.next()) {

			int id = rs.getInt("id");
			String nr = rs.getString("nr");
			int pat = rs.getInt("pat");
			int baie = rs.getInt("baie");
			int includeMasa = rs.getInt("include_masa");
			int cost = rs.getInt("cost");

			Camera s = new Camera(id,nr, pat, baie, includeMasa, cost);

			result.add(s);
		}
		DBHelper.closeConnection();
		return result;
	}
	
	public static void createCamera(String nr, int pat, int baie, int includeMasa, int cost) throws SQLException{
		String insert="insert into camera (nr, pat, baie,include_masa,cost)"
				+ "value(?,?,?,?,?);";
		
		Connection con = DBHelper.getConnection();
		
		PreparedStatement stmt = con.prepareStatement(insert);
		
		stmt.setString(1, nr);
		stmt.setInt(2,pat);
		stmt.setInt(3, baie);
		stmt.setInt(4,includeMasa);
		stmt.setInt(5, cost);
		
		stmt.executeUpdate();
		
		DBHelper.closeConnection();
	}
	
	public static void createCamera(Camera s) throws SQLException {
		createCamera(s.getNr(),s.getPat(),s.getBaie(),s.getIncludeMasa(),s.getCost());
	}
	
	public static void updateCamera(Camera s) throws SQLException {
		String update = "update camera set nr = ?, pat=?, baie=?,include_masa=?, cost=? where id=?";
	
		Connection con = DBHelper.getConnection();

		PreparedStatement stmt = con.prepareStatement(update);
		
		stmt.setString(1,s.getNr());
		stmt.setInt(2, s.getPat());
		stmt.setInt(3, s.getBaie());
		stmt.setInt(4, s.getIncludeMasa());
		stmt.setInt(5, s.getCost());
		stmt.setInt(6, s.getId());
		
		stmt.executeUpdate();
		
		DBHelper.closeConnection();
				
	}
	public static void deleteCamera(int id) throws SQLException{
		String delete = "delete from camera where id=?";
		
		Connection con = DBHelper.getConnection();

		PreparedStatement stmt = con.prepareStatement(delete);
		
		stmt.setInt(1, id);
		
		stmt.executeUpdate();
		
		DBHelper.closeConnection();
	}
	
	public static void deleteCamera(Camera s) throws SQLException {
		
		deleteCamera(s.getId());
	}
}
