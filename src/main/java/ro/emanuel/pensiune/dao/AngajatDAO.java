package ro.emanuel.pensiune.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import ro.emanuel.pensiune.helpers.DBHelper;
import ro.emanuel.pensiune.pojo.Angajat;

public class AngajatDAO {

	public static Angajat getAngajatById(int id) throws SQLException {
		
		String select = "select * from angajati where id=?";
		Connection con=DBHelper.getConnection();
		PreparedStatement stmt = con.prepareStatement(select);
		stmt.setInt(1, id);
		ResultSet rs =stmt.executeQuery();
		Angajat result = null;
		if (rs.next()) {
			String nume = rs.getString("nume");
			String prenume = rs.getString("prenume");
			String telefon = rs.getString("telefon");
			String email = rs.getString("email");
			String adresa = rs.getString("adresa");
			int salariu = rs.getInt("salariu");
			result = new Angajat(id, nume, prenume, telefon, email, adresa, salariu);
		}
		DBHelper.closeConnection();
		return result;
	}
	
	public static ArrayList<Angajat> getAngajatByName(String nameQuery) throws SQLException{
		String select = "select * from angajati where nume LIKE ?";
		Connection con = DBHelper.getConnection();
		PreparedStatement stmt = con.prepareStatement(select);
		stmt.setString(1, "%"+ nameQuery+"%");
		ArrayList<Angajat> result = new ArrayList<Angajat>();
		ResultSet rs = stmt.executeQuery();
		while (rs.next()) {

			int id = rs.getInt("id");
			String nume = rs.getString("nume");
			String prenume = rs.getString("prenume");
			String telefon = rs.getString("telefon");
			String email = rs.getString("email");
			String adresa = rs.getString("adresa");
			int salariu = rs.getInt("salariu");

			Angajat s = new Angajat(id, nume, prenume, telefon, email, adresa, salariu);

			result.add(s);
		}
		DBHelper.closeConnection();
		return result;
	}
	
	public static ArrayList<Angajat> getAngajati() throws SQLException {

		String select = "SELECT * FROM `angajati` ORDER BY `angajati`.`nume` ASC";

		Connection con = DBHelper.getConnection();

		PreparedStatement stmt = con.prepareStatement(select);

		ArrayList<Angajat> result = new ArrayList<Angajat>();
		ResultSet rs = stmt.executeQuery();
		while (rs.next()) {

			int id = rs.getInt("id");
			String nume = rs.getString("nume");
			String prenume = rs.getString("prenume");
			String telefon = rs.getString("telefon");
			String email = rs.getString("email");
			String adresa = rs.getString("adresa");
			int salariu = rs.getInt("salariu");

			Angajat s = new Angajat(id, nume, prenume, telefon, email, adresa, salariu);

			result.add(s);
		}
		DBHelper.closeConnection();
		return result;
	}
	
	public static void createAngajat(String nume,String prenume,String telefon, String email, String adresa, int salariu) throws SQLException{
		String insert="insert into angajati (nume,prenume,telefon,email,adresa,salariu)"
				+ "value(?,?,?,?,?,?);";
		
		Connection con = DBHelper.getConnection();
		
		PreparedStatement stmt = con.prepareStatement(insert);
		
		stmt.setString(1,nume);
		stmt.setString(2, prenume);
		stmt.setString(3,telefon);
		stmt.setString(4, email);
		stmt.setString(5, adresa);
		stmt.setInt(6, salariu);
		
		
		stmt.executeUpdate();
		
		DBHelper.closeConnection();
	}
	
	public static void createAngajat(Angajat s) throws SQLException {
		createAngajat(s.getNume(),s.getPrenume(),s.getTelefon(),s.getEmail(),s.getAdresa(),s.getSalariu());
	}
	
	public static void updateAngajat(Angajat s) throws SQLException {
		String update = "update angajati set nume=?, prenume=?,telefon=?, email=?,adresa=?,salariu=? where id=?";
	
		Connection con = DBHelper.getConnection();

		PreparedStatement stmt = con.prepareStatement(update);
		
		stmt.setString(1, s.getNume());
		stmt.setString(2, s.getPrenume());
		stmt.setString(3, s.getTelefon());
		stmt.setString(4, s.getEmail());
		stmt.setString(5, s.getAdresa());
		stmt.setInt(6, s.getSalariu());
		stmt.setInt(7, s.getId());
		
		stmt.executeUpdate();
		
		DBHelper.closeConnection();
				
	}
	public static void deleteAngajat(int id) throws SQLException{
		String delete = "delete from angajati where id=?";
		
		Connection con = DBHelper.getConnection();

		PreparedStatement stmt = con.prepareStatement(delete);
		
		stmt.setInt(1, id);
		
		stmt.executeUpdate();
		
		DBHelper.closeConnection();
	}
	
	public static void deleteAngajat(Angajat s) throws SQLException {
		
		deleteAngajat(s.getId());
	}
}
