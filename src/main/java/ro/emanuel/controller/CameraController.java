package ro.emanuel.controller;

import java.sql.SQLException;
import java.util.ArrayList;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import ro.emanuel.pensiune.dao.CameraDAO;
import ro.emanuel.pensiune.dao.ClientDAO;
import ro.emanuel.pensiune.pojo.Camera;
import ro.emanuel.pensiune.pojo.Client;

@Controller
public class CameraController {

	@RequestMapping(value="listcamera.htm",method=RequestMethod.GET)
	public ModelAndView listCamerele() throws SQLException {
		ModelMap model =new ModelMap();
		
		ArrayList<Camera> camerele=CameraDAO.getCamerele();
		model.put("camerele", camerele);
		return new ModelAndView("listcamera.jsp",model);
	}
	@RequestMapping(value="detaliicamera.htm", method=RequestMethod.GET)
	public ModelAndView details(@RequestParam Integer id) throws SQLException {
		ModelMap model = new ModelMap();
		Camera st = CameraDAO.getCameraById(id);
		model.put("camera", st);
		
		ArrayList<Client> clienti = ClientDAO.getActualClienti();
		for(Client c: clienti) {
			if(c.getIdCamera() == st.getId()) {
				model.put("idClient", c.getId());
				model.put("numeClient",c.getNume());
			}
		}
		
		return new ModelAndView("detaliicamera.jsp",model);
	}
	@RequestMapping(value="adaugare-camera.htm",method=RequestMethod.GET)
	public ModelAndView showAddCamera(Model model) throws SQLException {
		Camera st= new Camera();
		model.addAttribute("camera",st);
		
		return new ModelAndView("addcamera.jsp","model",model);
	}
	@RequestMapping(value="adaugare-camera-save.htm",method=RequestMethod.POST)
	public ModelAndView addCamera(@ModelAttribute("camera") Camera st,
			ModelMap model, BindingResult result) {
		try {
			CameraDAO.createCamera(st);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return new ModelAndView("redirect:/listcamera.htm");
		
	}
	@RequestMapping(value="deletecamera.htm", method=RequestMethod.GET)
	public ModelAndView deleteCamera(@RequestParam Integer id) throws SQLException {
		
		CameraDAO.deleteCamera(id);		
		return new ModelAndView("redirect:/listcamera.htm");
	}
	@RequestMapping(value="editare-camera.htm",method=RequestMethod.GET)
	public ModelAndView showEditCamera(@RequestParam Integer id,Model model) throws SQLException {
		Camera st= CameraDAO.getCameraById(id);
		model.addAttribute("camera",st);
		
		return new ModelAndView("editcamera.jsp","model",model);
	}
	@RequestMapping(value="editare-camera-save.htm",method=RequestMethod.POST)
	public ModelAndView editCamera(@ModelAttribute("camera") Camera st,
			ModelMap model, BindingResult result) {
		try {
			CameraDAO.updateCamera(st);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return new ModelAndView("redirect:/listcamera.htm");
		
	}
}
