package ro.emanuel.controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import ro.emanuel.pensiune.dao.CameraDAO;
import ro.emanuel.pensiune.dao.ClientDAO;
import ro.emanuel.pensiune.pojo.Camera;
import ro.emanuel.pensiune.pojo.Client;

@Controller
public class ClientController {

	@RequestMapping(value="listclient.htm",method=RequestMethod.GET)
	public ModelAndView listclienti() throws SQLException {
		ModelMap model =new ModelMap();
		
		ArrayList<Client> clienti=ClientDAO.getClienti();
		model.put("clienti", clienti);
		
		ArrayList<Camera> camerele = CameraDAO.getCamerele();
		HashMap<Integer, String> camereleMap = new HashMap<Integer, String>();
		for(Camera f: camerele) {
			camereleMap.put(f.getId(), f.getNr());
		}
		model.put("camerele", camereleMap);
		
		return new ModelAndView("listclient.jsp",model);
	}
	@RequestMapping(value="detaliiclient.htm", method=RequestMethod.GET)
	public ModelAndView detailsClient(@RequestParam Integer id) throws SQLException {
		ModelMap model = new ModelMap();
		Client st = ClientDAO.getClientById(id);
		model.put("client", st);
		
		ArrayList<Camera> camerele = CameraDAO.getCamerele();
		for(Camera f: camerele) {
			if(f.getId() == st.getIdCamera()) {
				model.put("nrCamera", f.getNr());
				model.put("cost",f.getCost());
			}
		}
		long days = ClientDAO.getDays(id);
			model.put("nrDays", days);
		
		return new ModelAndView("detaliiclient.jsp",model);
	}
	@RequestMapping(value="adaugare-client.htm",method=RequestMethod.GET)
	public ModelAndView showAddClient(Model model) throws SQLException {
		Client st= new Client();
		model.addAttribute("client",st);
		
		ArrayList<Camera> camerele = CameraDAO.getFreeCamerele();
		
		model.addAttribute("camerele",camerele);
		
		return new ModelAndView("addclient.jsp","model",model);
	}
	@RequestMapping(value="adaugare-client-save.htm",method=RequestMethod.POST)
	public ModelAndView addClient(@ModelAttribute("client") Client st,
			ModelMap model, BindingResult result) {
		try {
			ClientDAO.createClient(st);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return new ModelAndView("redirect:/listclient.htm");
		
	}
	@RequestMapping(value="deleteclient.htm", method=RequestMethod.GET)
	public ModelAndView deleteClient(@RequestParam Integer id) throws SQLException {
		
		ClientDAO.deleteClient(id);		
		return new ModelAndView("redirect:/listclient.htm");
	}
	@RequestMapping(value="editare-client.htm",method=RequestMethod.GET)
	public ModelAndView showEditClient(@RequestParam Integer id,Model model) throws SQLException {
		Client st= ClientDAO.getClientById(id);
		model.addAttribute("client",st);
		
		ArrayList<Camera> camerele = CameraDAO.getFreeCamerele();
		ArrayList<Camera> camerele2 = CameraDAO.getCamerele();
		
		for(Camera c: camerele2)
				if(c.getId() == st.getIdCamera())
				camerele.add(c);
		
		model.addAttribute("camerele",camerele);
		
		
		return new ModelAndView("editclient.jsp","model",model);
	}
	@RequestMapping(value="editare-client-save.htm",method=RequestMethod.POST)
	public ModelAndView editClient(@ModelAttribute("client") Client st,
			ModelMap model, BindingResult result) {
		try {
			ClientDAO.updateClient(st);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return new ModelAndView("redirect:/listclient.htm");
		
	}
}
