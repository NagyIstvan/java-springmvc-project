package ro.emanuel.controller;

import java.sql.SQLException;
import java.util.ArrayList;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import ro.emanuel.pensiune.dao.AngajatDAO;
import ro.emanuel.pensiune.dao.CameraDAO;
import ro.emanuel.pensiune.dao.ClientDAO;
import ro.emanuel.pensiune.pojo.Angajat;

@Controller
public class AngajatController {

	@RequestMapping(value="listangajat.htm",method=RequestMethod.GET)
	public ModelAndView listangajati() throws SQLException {
		ModelMap model =new ModelMap();
		
		ArrayList<Angajat> angajati=AngajatDAO.getAngajati();
		model.put("angajati", angajati);
		return new ModelAndView("listangajat.jsp",model);
	}
	@RequestMapping(value="detaliiangajat.htm", method=RequestMethod.GET)
	public ModelAndView detailsAngajat(@RequestParam Integer id) throws SQLException {
		ModelMap model = new ModelMap();
		Angajat st = AngajatDAO.getAngajatById(id);
		model.put("angajat", st);
		
		return new ModelAndView("detaliiangajat.jsp",model);
	}
	@RequestMapping(value="adaugare-angajat.htm",method=RequestMethod.GET)
	public ModelAndView showAddAngajat(Model model) throws SQLException {
		Angajat st= new Angajat();
		model.addAttribute("angajat",st);
		
		return new ModelAndView("addangajat.jsp","model",model);
	}
	@RequestMapping(value="adaugare-angajat-save.htm",method=RequestMethod.POST)
	public ModelAndView addAngajat(@ModelAttribute("angajat") Angajat st,
			ModelMap model, BindingResult result) {
		try {
			AngajatDAO.createAngajat(st);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return new ModelAndView("redirect:/listangajat.htm");
		
	}
	@RequestMapping(value="deleteangajat.htm", method=RequestMethod.GET)
	public ModelAndView deleteAngajat(@RequestParam Integer id) throws SQLException {
		
		AngajatDAO.deleteAngajat(id);		
		return new ModelAndView("redirect:/listangajat.htm");
	}
	@RequestMapping(value="editare-angajat.htm",method=RequestMethod.GET)
	public ModelAndView showEditAngajat(@RequestParam Integer id,Model model) throws SQLException {
		Angajat st= AngajatDAO.getAngajatById(id);
		model.addAttribute("angajat",st);
		
		return new ModelAndView("editangajat.jsp","model",model);
	}
	@RequestMapping(value="editare-angajat-save.htm",method=RequestMethod.POST)
	public ModelAndView editAngajat(@ModelAttribute("angajat") Angajat st,
			ModelMap model, BindingResult result) {
		try {
			AngajatDAO.updateAngajat(st);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return new ModelAndView("redirect:/listangajat.htm");
		
	}
	@RequestMapping(value="/", method=RequestMethod.GET)
	public ModelAndView index() throws SQLException {
		
		return new ModelAndView("redirect:/index.htm");
	}
	@RequestMapping(value="index.htm", method=RequestMethod.GET)
	public ModelAndView dataIndex() throws SQLException {
		ModelMap model =new ModelMap();
		int clientActual = ClientDAO.getActualClienti().size();
		model.put("clientActual", clientActual);
		int clientTotal = ClientDAO.getClienti().size();
		model.put("clientTotal", clientTotal);
		int cameraGoala = CameraDAO.getFreeCamerele().size();
		model.put("cameraGoala", cameraGoala);
		int cameraTotal = CameraDAO.getCamerele().size();
		model.put("cameraTotal", cameraTotal);
		
		return new ModelAndView("index.jsp",model);
	}
}
